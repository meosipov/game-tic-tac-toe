import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

import TicTacToe 1.0

Rectangle {
    id: root

    property alias difficulty: bot.difficulty

    color: {
        if (field.winner === 0)
            return "transparent"

        return field.winner === root.player ? "#224422" : "#442222"
    }
    Behavior on color { ColorAnimation {} }

    GameField {
        id: field
        size: 3
        onWinnerChanged: {
            console.log('Winner: ' + winner)
        }
    }

    GameBot {
        id: bot
        field: field
        player: 3 - root.player
        onDifficultyChanged: {
            console.log('Difficulty changed to: ' + difficulty)
        }
    }

    /// User player. If user player is equal 1 then he started first
    property int player: 0
    onPlayerChanged: console.log("User set to player: " + player)

    Timer {
        id: timer
        interval: 10
        repeat: false
        onTriggered: {
            /// Randomizing player
            player = Math.floor(Math.random() * 2) + 1

            /// Bot step first
            if (player == 2) {
                bot.step()
            }
        }
    }

    /// This treek is needed because difficulty will be changed later
    Component.onCompleted: timer.start()

    GridLayout {
        anchors.centerIn: parent

        columns: field.size
        rows: field.size
        flow: GridLayout.LeftToRight

        columnSpacing: 10
        rowSpacing: 10

        Repeater {
            Layout.alignment: Layout.Center

            model: field
            delegate: Rectangle {
                id: item

                required property int edit
                required property string display
                required property int row
                required property int column

                width: 100
                height: 100

                Text {
                    id: text
                    anchors.centerIn: parent
                    color: display === "1" ? Material.primaryColor : Material.accentColor
                    text: {
                        if (display === "1")
                            return "×"
                        else if (display === "2")
                            return "○"
                        else if (display === "0")
                            return ""
                        return "?"
                    }

                    font.weight: Font.Thin
                    font.pixelSize: 75

                    onTextChanged: PropertyAnimation { target: text; property: "scale"; from: 0.2; to: 1; easing.type: Easing.OutBack }
                }

                color: {
                    if (!area.containsMouse)
                        return "transparent"
                    return display === "0" ? Material.accentColor : Material.buttonDisabledColor
                }
                Behavior on color { ColorAnimation {} }

                border.color: Material.frameColor
                border.width: display === "0" ? 2 : 0

                radius: 10

                MouseArea {
                    id: area
                    anchors.fill: parent
                    hoverEnabled: true
                    acceptedButtons: Qt.LeftButton

                    onClicked: {
                        var index = field.index(row, column);
                        /// Checking here, becasue flags method is not working :`(
                        if (field.data(index) !== "0")
                            return

                        if (mouse.button == Qt.LeftButton)
                            field.setData(index, player, Qt.EditRole)

                        bot.step()
                    }
                }
            }
        }
    }
}
