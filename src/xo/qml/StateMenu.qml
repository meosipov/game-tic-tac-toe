import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Item {
    id: root

    property Loader loader: null

    Action {
        id: actionRookie
        text: qsTr("Rookie")
        shortcut: "0"
        onTriggered: {
            loader.source = "qrc:/qml/qml/StateGame.qml"
            loader.item.difficulty = 0
        }
    }

    Action {
        id: actionEasy
        text: qsTr("Easy")
        shortcut: "1"
        onTriggered: {
            loader.source = "qrc:/qml/qml/StateGame.qml"
            loader.item.difficulty = 1
        }
    }

    Action {
        id: actionMedium
        text: qsTr("Medium")
        shortcut: "2"
        onTriggered: {
            loader.source = "qrc:/qml/qml/StateGame.qml"
            loader.item.difficulty = 2
        }
    }

    Action {
        id: actionHard
        text: qsTr("Hard")
        shortcut: "3"
        onTriggered: {
            loader.source = "qrc:/qml/qml/StateGame.qml"
            loader.item.difficulty = 3
        }
    }

    Image {
        anchors.centerIn: parent
        source: "qrc:/images/images/background.png"

        opacity: 0.05
        scale: 2
    }

    Text {
        text: qsTr("Select difficulty")
        color: Material.foreground
        font.pixelSize: 80

        anchors.bottom: buttonLayout.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ColumnLayout {
        id: buttonLayout
        anchors.centerIn: parent

        Repeater {
            /// It could be text here in model instead of actions, but actions
            /// live longer and property setup is works
            model: [actionRookie, actionEasy, actionMedium, actionHard]

            delegate: Button {
                scale: 2

                Layout.margins: 30
                Layout.alignment: Layout.Center
                Layout.fillWidth: true

                action: modelData
                text: modelData.text
            }
        }
    }
}
