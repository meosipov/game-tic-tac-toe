import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12

ApplicationWindow {
    id: root
        // TODO: Add version
    title: qsTr("Tic Tac Toe")

    Material.theme: Material.Dark
    Material.accent: Material.Orange

    width: 1280
    height: 800
    minimumWidth: 720
    minimumHeight: 520

    visible: true

    Action {
        shortcut: "Esc"
        onTriggered: loader.source = "qrc:/qml/qml/StateMenu.qml"
    }

    Loader {
        id: loader
        anchors.fill: parent
        source: "qrc:/qml/qml/StateMenu.qml"
        onSourceChanged: console.log("Loading: " + source)
        onLoaded: {
            if (item.loader !== undefined)
                item.loader = this
        }
    }
}
