# This file is part of the Tic Tac Toe game
# (https://gitlab.com/meosipov/game-tic-tac-toe).
# Copyright (c) 2021 Mikhail Osipov.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from PySide2.QtCore import QModelIndex, QAbstractListModel, Property, Signal
from PySide2.QtCore import Qt


class Field(QAbstractListModel):
    """
    Field is a field controller class
    """

    def __init__(self, _size=0, parent=None):
        super(Field, self).__init__(parent)
        self._cells = np.array([], dtype=int)
        self._winner = 0
        self.size = _size

    sizeChanged = Signal()
    listChanged = Signal()
    winnerChanged = Signal()

    @Property(int, notify=sizeChanged)
    def size(self) -> int:
        """
        Size property
        """
        return self._size

    @size.setter
    def size(self, __size: int):
        """
        Size property setter

        :param __size: field size
        """
        self._size = __size
        self._cells.resize(self._size ** 2)
        self._cells.fill(0)
        self.sizeChanged.emit()

    @Property(list, notify=listChanged)
    def cells(self) -> list:
        """
        cells property
        """
        return list(self._cells)

    @Property(int, notify=winnerChanged)
    def winner(self) -> int:
        """
        Match winner
        """
        return self._winner

    def position(self, row: int, col: int) -> int:
        """
        Calculates cell position in array
        """
        return self._size * row + col

    def generate_slices(self) -> list:
        """
        Generating field slices
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 4, 8],
        [2, 4, 6],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8]
        """
        for i in range(0, self._size):
            yield list(range(i * self._size, i * self._size + self._size))
            yield [j * self._size + i for j in range(0, self._size)]

        yield [i * (self._size + 1) for i in range(0, self._size)]
        yield [(self._size - 1) * (i + 1) for i in range(0, self._size)]

    def check_winner(self):
        """
        Checking winner
        """
        if self._winner:
            return

        for s in self.generate_slices():
            values = np.array([self._cells[cell] for cell in s])
            if np.all(values == values[0]) and values[0]:
                self._winner = values[0]
                self.winnerChanged.emit()
                return

    # Model methods
    def rowCount(self, parent=QModelIndex()) -> int:
        return len(self._cells)

    def columnCount(self, parent=QModelIndex()) -> int:
        return 1

    def flags(self, index) -> Qt.ItemFlags:
        print('Accessing flags')
        _flags = QAbstractListModel.flags(index)
        if index.isValid() and self._cells[index.row()] and not self._winner:
            _flags |= Qt.ItemIsEditable | Qt.ItemIsSelectable
        return _flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        player = self._cells[index.row()]
        if role == Qt.DisplayRole:
            return str(player)

        return None

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or self._winner:
            return False

        if role == Qt.EditRole:
            self._cells[index.row()] = value
        else:
            return False

        self.dataChanged.emit(index, index, [Qt.DisplayRole])
        self.check_winner()
        return True
