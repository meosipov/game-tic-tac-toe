# This file is part of the Tic Tac Toe game
# (https://gitlab.com/meosipov/game-tic-tac-toe).
# Copyright (c) 2021 Mikhail Osipov.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import random as rnd

import numpy as np
from PySide2.QtCore import QObject, Property, Signal, Slot
from PySide2.QtQuick import QQuickItem

from lib.field import Field


class Bot(QQuickItem):
    """
    Bot class

    This class is used for game simulation by PC player
    """

    def __init__(self, parent=None):
        super(Bot, self).__init__(parent)
        self._player = None
        self._field = None
        self._difficulty = 1

    fieldChanged = Signal()
    playerChanged = Signal()
    difficultyChanged = Signal()

    @Property(int, notify=playerChanged)
    def player(self) -> int:
        """
        Player property
        """
        return self._player

    @player.setter
    def player(self, player: int):
        """
        Player property setter

        :param player: Player id
        """
        self._player = player
        self.playerChanged.emit()

    @Property(QObject, notify=fieldChanged)
    def field(self) -> Field:
        """
        Field property
        """
        return self._field

    @field.setter
    def field(self, field: Field):
        """
        Field property setter
        :param field: Field link
        """
        self._field = field
        self.fieldChanged.emit()

    @Property(int, notify=difficultyChanged)
    def difficulty(self) -> int:
        """
        Property difficulty
        """
        return self._difficulty

    @difficulty.setter
    def difficulty(self, diff: int):
        """
        Set difficulty

        :param diff: Difficulty level [0, 1, 2, 3]
        """
        self._difficulty = diff
        self.difficultyChanged.emit()

    @Slot()
    def step(self):
        """
        Perform bot step operation
        """
        if not self._field:
            raise RuntimeError('Failed to perform step: No field provided.')
        elif not self._player:
            raise RuntimeError('Failed to perform step: Player is not set.')

        cells = self._field.cells
        # Rookie
        if self._difficulty == 0:
            self.random_step(cells)
        # Easy
        elif self._difficulty == 1:
            # Checking win step and opponent potential win step
            if not self.slice_step(cells, self._player) and not \
                    self.slice_step(cells, 3 - self._player):
                self.random_step(cells)
        else:
            RuntimeError('Failed to perform step: Not implemented')

    def random_step(self, _cells: list):
        """
        Simple random step

        :param _cells: List of cells
        """
        estimate = [i for i in range(0, len(_cells)) if _cells[i] == 0]
        if not len(estimate):
            return

        gamble = rnd.randint(0, len(estimate) - 1)
        cell = estimate[gamble]
        self._field.setData(self._field.index(cell, 0), self._player)

    def slice_step(self, _cells: list, player: int) -> bool:
        """
        This method is used to search double occurrence of provided player in
        slice and make step there. It could be current player or opponent
        depending on strategy

        :param _cells: List of estimated cells
        :param player: Searched id
        """
        slices = self._field.generate_slices()
        # Analyzing slices
        for s in slices:
            score = [_cells[cell] for cell in s]
            if score.count(player) == 2 and score.count(0):
                cell = np.where(np.array(score) == 0)[0][0]
                index = self._field.index(s[cell], 0)
                self._field.setData(index, self._player)
                return True

        return False
