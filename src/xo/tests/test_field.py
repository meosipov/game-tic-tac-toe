# This file is part of the Tic Tac Toe game
# (https://gitlab.com/meosipov/game-tic-tac-toe).
# Copyright (c) 2021 Mikhail Osipov.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pytest

from lib.field import Field


class TestField:
    """
    Field testing unit
    """

    def setup_method(self):
        self._field = Field(3)

    @pytest.mark.parametrize("size", [2, 4, 9])
    def test_size(self, size: int):
        self._field.size = size
        assert self._field.size == size

    @pytest.mark.parametrize("field, cells", [
        (Field(1), 1),
        (Field(2), 4),
        (Field(3), 9)
    ])
    def test_cells(self, field: Field, cells: int):
        self._field = field
        assert len(self._field._cells) == cells

    @pytest.mark.parametrize("winner", [1, 2])
    def test_winner(self, winner: int):
        self._field._winner = winner
        assert self._field.winner == winner

    @pytest.mark.parametrize("row, column, index", [
        (0, 0, 0),
        (0, 1, 1),
        (0, 2, 2),
        (1, 0, 3),
        (1, 1, 4),
        (1, 2, 5)
    ])
    def test_position(self, row: int, column: int, index: int):
        assert self._field.position(row, column) == index

    def test_slices(self):
        expected = sorted([
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 4, 8],
            [2, 4, 6],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8]
        ])
        slices = sorted(list(self._field.generate_slices()))
        assert slices == expected

    @pytest.mark.parametrize("data, expected", [
        ([1, 0, 0, 0, 1, 0, 0, 0, 1], 1),
        ([2, 0, 0, 2, 0, 0, 2, 0, 0], 2),
        ([1, 2, 3, 1, 3, 0, 3, 2, 1], 3)
    ])
    def test_winner(self, data, expected):
        self._field._cells = data
        self._field.check_winner()
        assert self._field.winner == expected

    def test_model(self):
        assert self._field.rowCount() == 9
        assert self._field.columnCount() == 1
        assert self._field.data(self._field.index(0, 0)) == "0"

        self._field.setData(self._field.index(0, 0), 1)
        assert self._field.data(self._field.index(0, 0)) == "1"
