# This file is part of the Tic Tac Toe game
# (https://gitlab.com/meosipov/game-tic-tac-toe).
# Copyright (c) 2021 Mikhail Osipov.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pytest

from lib.bot import Bot
from lib.field import Field


class TestBot:
    """
    Field testing unit
    """

    def setup_method(self):
        self._bot = Bot()
        self._field = Field(3)
        self._bot.field = self._field
        self._bot.player = 1

    @pytest.mark.parametrize("player", [1, 2, 3])
    def test_player(self, player: int):
        self._bot.player = player
        assert self._bot.player == player

    @pytest.mark.parametrize("field", [Field(1), Field(2), Field(3)])
    def test_field(self, field: Field):
        self._bot.field = field
        assert self._bot.field == field

    @pytest.mark.parametrize("difficulty", [0, 1, 2, 3])
    def test_difficulty(self, difficulty: int):
        self._bot.difficulty = difficulty
        assert self._bot.difficulty == difficulty

    @pytest.mark.parametrize("field, player, action_player, result", [
        ([2, 2, 0, 0, 0, 1, 0, 1, 1], 1, 2, [2, 2, 1, 0, 0, 1, 0, 1, 1]),  # Prevent step
        ([2, 2, 0, 0, 0, 1, 0, 1, 1], 2, 2, [2, 2, 2, 0, 0, 1, 0, 1, 1])  # Act step
    ])
    def test_step(self, field: list, player: int, action_player: int, result: list):
        self._field._cells = field
        self._bot.player = player
        assert self._bot.slice_step(self._field.cells, action_player) is True
        assert self._field.cells == result
