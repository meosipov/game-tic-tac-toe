#!/usr/bin/env python3

# This file is part of the Tic Tac Toe game
# (https://gitlab.com/meosipov/game-tic-tac-toe).
# Copyright (c) 2021 Mikhail Osipov.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys

from PySide2.QtGui import QGuiApplication, QIcon
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType

from lib.bot import Bot
from lib.field import Field
import resources


if __name__ == "__main__":
    qmlRegisterType(Field, 'TicTacToe', 1, 0, "GameField")
    qmlRegisterType(Bot, 'TicTacToe', 1, 0, "GameBot")

    sys.argv += ['--style', 'material']
    app = QGuiApplication(sys.argv)
    app.setWindowIcon(QIcon(":/images/images/icon.png"))
    engine = QQmlApplicationEngine()
    engine.load(":/qml/qml/mainwindow.qml")

    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec_())
