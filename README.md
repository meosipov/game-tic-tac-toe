# Tic Tac Toe 

It is a paper-and-pencil game for two players, X and O, who take turns marking the spaces in a 3×3 grid. The player who succeeds in placing three of their marks in a diagonal, horizontal, or vertical row is the winner. It is a solved game with a forced draw assuming best play from both players. 
https://en.wikipedia.org/wiki/Tic-tac-toe

## Description
This game is written on Python and QML

## Interface

### Menu

![Main menu](readme/menu.png)

Player can select difficulty level at main manu.


### Game field

![Game field](readme/field.png)

Game interface is a grid 3x3 cells. Player selects cell 

### Gameplay

At the beginning application choose who will start first. If first player is a PC, then it makes first move. Otherwise grid will be empty.

![Winner](readme/easy-win.gif)

![Looser](readme/easy-fail.gif)
